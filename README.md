# Softcheck
Softcheck is a platform that allows a modular definitions of static program
analyses arbitrarily complex, focused on data flow analyses. Benefiting from
an expressive module system, the platform allows the definition and execution
of analyses in a way abstract to the source code language of the program,
making the process simpler and the analyses easily extensible and reusable.
